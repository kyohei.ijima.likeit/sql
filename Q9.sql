SELECT
item_category.category_name,
SUM(item.item_price) as total_price
FROM
item
INNER JOIN
item_category
ON
    item.category_id = item_category.category_id
GROUP BY
    item_category.category_name
order by total_price desc
